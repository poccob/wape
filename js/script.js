/* animation button go to catalog main screen */

$(".menu__item--center-link").mouseenter(function(event) {
  $(".menu__line--top").css("top", "-370%");
  $(".menu__line--bottom").css("top", "120%");
});
$(".menu__item--center-link").mouseleave(function(event) {
  $(".menu__line--top").css("top", "-550%");
  $(".menu__line--bottom").css("top", "300%");
});


/* popup button main screen catalog */

$(".ms-item-1").hover(function(event) {
  $(".ms-item-1").css("height", "520px");
  setTimeout(function(){
    $(".ms-item-button-1").css("opacity", "1");
  }, 400);
  }, function(){
    setTimeout(function(){
      $(".ms-item-1").css("height", "430px");
      $(".ms-item-button-1").css("opacity", "0");
    }, 400);
});

$(".ms-item-2").hover(function(event) {
  $(".ms-item-2").css("height", "520px");
  setTimeout(function(){
    $(".ms-item-button-2").css("opacity", "1");
  }, 400);
  }, function(){
    setTimeout(function(){
      $(".ms-item-2").css("height", "430px");
      $(".ms-item-button-2").css("opacity", "0");
    }, 400);
});

$(".ms-item-3").hover(function(event) {
  $(".ms-item-3").css("height", "520px");
  setTimeout(function(){
    $(".ms-item-button-3").css("opacity", "1");
  }, 400);
  }, function(){
    setTimeout(function(){
      $(".ms-item-3").css("height", "430px");
      $(".ms-item-button-3").css("opacity", "0");
    }, 400);
});

/* animation button go to catalog main screen */

$(".ms-catalog__link").mouseenter(function(event) {
  $(".ms-catalog__line--top").css("top", "-23%");
  $(".ms-catalog__line--bottom").css("top", "8%");
});
$(".ms-catalog__link").mouseleave(function(event) {
  $(".ms-catalog__line--top").css("top", "-33%");
  $(".ms-catalog__line--bottom").css("top", "18%");
});

/* animation button go to reviews */

$(".reviews__link").mouseenter(function(event) {
  $(".reviews__line--top").css("top", "-22%");
  $(".reviews__line--bottom").css("top", "8%");
});
$(".reviews__link").mouseleave(function(event) {
  $(".reviews__line--top").css("top", "-30%");
  $(".reviews__line--bottom").css("top", "18%");
});
